using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;

namespace Mechanics {
    public class GameManager : AbstarctEnablingSigleton<GameManager> {
        public StatData[] StatsData;

        public Card[] Cards;
        
        public CardAmount[] StartingHand;
        
        private List<Stat> _stats;

        private List<Card> _hand;
        
        private void Start() {
            _hand = new List<Card>();
            _stats = StatsData.Select(data => data.GetStat()).ToList();
            foreach (var cardAmount in StartingHand) {
                DrawCardAmount(cardAmount);
            }
        }

        private void DrawCardAmount(CardAmount cardAmount) {
            for (int i = 0; i < cardAmount.Amount; i++) {
                DrawCard(cardAmount.CardSuit);
            }
        }

        public void PlayCard(Card card) {
            if (!ConditionsMet(card)) {
                Debug.Log("Can't pay cost");
                return;
            }
            
            PayCost(card);
            
            foreach (var cardAffection in card.Affections) {
                var stat = cardAffection.StatData.GetStat();
                stat.Affect(cardAffection.Diff);
            }
            
            RemoveCard(card);
            SpawnGranted(card);
        }

        public void PayCost(Card card) {
            foreach (var cardCost in card.Cost) {
                var canditates = _hand.Where(c => c != card).Where(c => c.Suit.Equals(cardCost.CardSuit)).ToList();

                for (int i = 0; i < cardCost.Amount; i++) {
                    var removee = canditates[Random.Range(0, canditates.Count)];
                    canditates.Remove(removee);
                    RemoveCard(removee);
                }
            }
        }

        private void RemoveCard(Card removee) {
            _hand.Remove(removee);
            Destroy(removee.gameObject);
        }

        public void SpawnGranted(Card card) {
            foreach (var cardType in card.Grants) {
                DrawCardAmount(cardType);
            }
        }
        
        public bool ConditionsMet(Card card) {
            foreach (var cardCost in card.Cost) {
                var cost = cardCost;
                if (cardCost.Amount > _hand.Where(c => c != card).Count(c => c.Suit.Equals(cardCost.CardSuit))) {
                    return false;
                }
            }

            return true;
        }

        private void DrawCard(CardSuit suit) {
            var candidates = Cards.Where(card => card.Suit == suit).ToList();
            var newCard = Instantiate(candidates[Random.Range(0, candidates.Count)]);
            _hand.Add(newCard);
            
            UiManager.GetInstance().AddCard(newCard);
        }
    }
}