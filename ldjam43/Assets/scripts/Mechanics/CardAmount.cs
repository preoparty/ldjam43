﻿using System;

namespace Mechanics {
    [Serializable]
    public class CardAmount {
        public CardSuit CardSuit;
        public int Amount;
    }
}