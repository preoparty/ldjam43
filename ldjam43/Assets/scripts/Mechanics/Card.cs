﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Mechanics {
	public class Card : MonoBehaviour {
		public StatAffection[] Affections;

		public CardSuit Suit;

		public CardAmount[] Cost;
		public CardAmount[] Grants;

		public void Play() {
			GameManager.GetInstance().PlayCard(this);
		}
		
		[Serializable]
		public class StatAffection {
			public StatData StatData;
			public int Diff;
		}
	}
}
