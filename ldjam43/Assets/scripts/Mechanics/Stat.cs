using UnityEngine;

namespace Mechanics {
    public class Stat {
        private int MinValue;
        private int MaxValue;
        public Stat(int minValue, int maxValue, int startValue) {
            MaxValue = maxValue;
            MinValue = minValue;
            Value = startValue;
        }

        public int Value { get; private set; }

        public void Affect(int diff) {
            Value += diff;
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
        }
        
    }
}