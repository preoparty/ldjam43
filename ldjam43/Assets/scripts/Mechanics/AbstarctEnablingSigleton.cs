﻿using System;
using System.Linq;
using UnityEngine;

namespace Mechanics {
    public abstract class AbstarctEnablingSigleton<T> : MonoBehaviour where T : MonoBehaviour {
        private static T _instance;

        public static T GetInstance() {
            if (_instance != null && _instance.enabled) {
                return _instance;
            }

            var instances = FindObjectsOfType<T>();
            var enabledInstances = instances.Where(obj => obj.enabled).ToList();

            var count = enabledInstances.Count;
        
            if (count > 1) {
                
                throw new InvalidOperationException("Found " + count + " objects of type " + typeof(T).Name + ". Should be only one.");
            }

            if (count == 0) {
                throw new InvalidOperationException("Could not find object of singleton " + typeof(T).Name);
            }

            _instance = enabledInstances.First();

            return _instance;
        }

        private void OnDisable() {
            if (this.Equals(_instance)) {
                _instance = null;
            }
        }

        public static bool IsPresent() {
            if (_instance != null) {
                return true;
            }
            return FindObjectsOfType<T>().Length > 0;
        }
    }
}