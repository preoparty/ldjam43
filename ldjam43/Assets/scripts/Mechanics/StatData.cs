﻿using System;
using UnityEngine;

namespace Mechanics {
    [CreateAssetMenu(fileName = "New Stat", menuName = "New Stat", order = 51)]
    public class StatData : ScriptableObject {
        [SerializeField] public int MaxValue;
        [SerializeField] public int MinValue;
        [SerializeField] public int StartValue;
        [SerializeField] public string name;

        private Stat Stat;
        
        public Stat GetStat() {
            if (Stat == null) {
                Stat = new Stat(MinValue, MaxValue, StartValue);
            }
            return Stat;
        }
    }
}