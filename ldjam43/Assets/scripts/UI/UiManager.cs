using System;
using Mechanics;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
    public class UiManager : AbstarctEnablingSigleton<UiManager> {
        public StatText[] StatTexts;

        public Transform CardGroup;
        public Text CardTextLinePrefab;
        
        private void Update() {
            foreach (var statText in StatTexts) {
                statText.Text.text = statText.StatData.GetStat().Value.ToString();
            }
        }


        public void AddCard(Card card) {
            card.transform.SetParent(CardGroup);
            var panel = card.GetComponentInChildren<VerticalLayoutGroup>();

            foreach (var cardAffection in card.Affections) {
                var text = Instantiate(CardTextLinePrefab);
                text.transform.SetParent(panel.transform);
                text.text = cardAffection.StatData.name + " " + (Math.Sign(cardAffection.Diff) > 0 ? "+" : "-");
            }
        }
        
        [Serializable]
        public class StatText {
            public StatData StatData;
            public Text Text;
        }
    }
}