﻿using UnityEngine;

namespace Stealth {
	public class EnemyDeathTrigger : MonoBehaviour {
		public static int KilledCount;
		public static int MaxCount;

		public int PortalActivationCount = 1;
		public NextLevelPortal Portal;
		
		private void Start() {
			KilledCount = 0;
			Portal.gameObject.SetActive(false);
			MaxCount = PortalActivationCount;
		}

		private void OnTriggerEnter(Collider other) {
			if (other.GetComponent<Navigator>()) {
				Destroy(other.gameObject);
				KilledCount++;

				if (KilledCount >= PortalActivationCount) {
					UiManager.GetInstance().OnVictory();
				}
			}
		}
	}
}
