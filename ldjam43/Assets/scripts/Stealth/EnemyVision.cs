﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour {
	public Navigator navigator;

	public float DetectionTime = 3.0f;
	public float DetectionIncreaseDistance = 5.0f;
	public float DetectionIncreseFactor = 1.0f;
	private LineRenderer line;
	private float _detectionTime = -1;
	private bool _detected = false;

	void Start() {
		line = GetComponent<LineRenderer> ();
	}

	void OnTriggerStay(Collider other) {
		if (_detected) return;
		if (!other.CompareTag("Player")) return;

		Vector3 dirToTarget = (other.gameObject.transform.position - transform.position).normalized;

		RaycastHit hit;
		Ray ray = new Ray (transform.position, dirToTarget);

		if (Physics.Raycast (ray, out hit)) {
			if (hit.collider.gameObject.CompareTag("Player")) {
				line.SetPosition(0, transform.position);
				line.SetPosition(1, hit.point);
				OnPlayerDetected(other.transform, false);
				return;
			}
		}
		SetColor(Color.white);
		ResetLine ();
		_detectionTime = -1;
	}

	private void OnTriggerExit(Collider other) {
		if (_detected) return;
		if (!other.CompareTag("Player")) return;
		SetColor(Color.white);
		GetComponentInParent<Voice>().OnLost();
		ResetLine ();
		_detectionTime = -1;
	}

	public void OnPlayerDetected(Transform character, bool force) {
		if (_detectionTime < 0 && !force) {
			GetComponentInParent<Voice> ().OnFound();
			SetColor(Color.yellow);
			_detectionTime = Time.time;
			return;
		}

		var distance = Vector3.Distance(transform.position, character.position);

		var factor = 1.0f;
		if (distance < DetectionIncreaseDistance) {
			factor = (DetectionIncreaseDistance - distance) / DetectionIncreaseDistance / DetectionIncreseFactor;
		}

		if (Time.time - _detectionTime < DetectionTime * factor && !force) return;

		SetColor(Color.red);
		ResetLine();
		navigator.SetTarget(character);
		_detected = true;

	}

	private void SetColor(Color color) {
		var meshRenderer = GetComponent<MeshRenderer>();
		meshRenderer.materials[0].SetColor("_Color", color);
	}

	public void Reset() {
		SetColor(Color.white);
		ResetLine ();
		_detectionTime = -1;
		_detected = false;
		ResetLine ();
	}

	public void ResetLine() {
		line.SetPosition(0, new Vector3(0,0,0));
		line.SetPosition(1, new Vector3(0,0,0));
	}

	void OnTriggerEnter(Collider other) {
	}
}
