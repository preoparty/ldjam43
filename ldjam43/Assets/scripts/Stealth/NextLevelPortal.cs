using UnityEngine;
using UnityEngine.SceneManagement;

namespace Stealth {
    public class NextLevelPortal : MonoBehaviour {
        public string NextLevelScene;

        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag("Player")) {
                SceneManager.LoadScene(NextLevelScene);
            }
        }
    }
}