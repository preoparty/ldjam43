﻿using UnityEngine;

namespace Stealth {
    public class Projectile : MonoBehaviour {
        public float Lifetime = 5.0f;

        public int Damage = 10;
        public float DamageVelocity = 10;
        public bool DestroyOnCollision = true;
        private float _spawnTime;

        void Start() {
            _spawnTime = Time.time;
        }

        // Update is called once per frame
        void Update() {
            if (Time.time - _spawnTime > Lifetime) Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision other) {
            if (other.collider.CompareTag("Player")) {
                if (GetComponent<Rigidbody>().velocity.magnitude > DamageVelocity) {
                    other.collider.GetComponent<Character>().TakeDamage(this);
                }
            }

            if (DestroyOnCollision) {
                Destroy(gameObject);
            }
        }
    }
}