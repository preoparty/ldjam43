using Mechanics;
using UnityEngine;

namespace Stealth {
    public class MusicManager : AbstarctEnablingSigleton<MusicManager> {
        public AudioClip HoldCLip;
        public AudioClip ChaseClip;
        public AudioClip DefaultClip;
        private AudioSource _audio;

        private bool _hold = true;

        private void Start() {
            _audio = GetComponent<AudioSource>();
            PlayDefault();
        }


        public void PlayDefault() {
            PlayClip(DefaultClip);
        }

        public void PlayChase() {
            if (_audio.clip != ChaseClip) {
                PlayClip(ChaseClip);
            }
        }

        public void StopChase() {
            if (IsChasing()) {
                return;
            }

            if (IsHolding()) {
                PlayHold();
                return;
            }

            PlayClip(DefaultClip);
        }

        private static bool IsChasing() {
            foreach (var navigator in FindObjectsOfType<Navigator>()) {
                if (navigator.IsChasing()) return true;
            }

            return false;
        }

        private static bool IsHolding() {
            return Character.GetInstance().IsHoldingAnything();
        }

        public void PlayHold() {
            if (!IsChasing() && IsHolding()) {
                PlayClip(HoldCLip);
            }
        }

        public void Drop() {
            if (!IsChasing()) {
                _audio.Stop();
                PlayDefault();
            }
        }

        private void PlayClip(AudioClip clip) {
            if (_audio.clip == clip) return;

            _audio.clip = clip;
            _audio.Play();
            _audio.loop = true;
        }
    }
}