﻿using Mechanics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Stealth {
    public class Character : AbstarctEnablingSigleton<Character> {
        public float Speed = 5.0f;
        public float HoldSpeed = 5.0f;
        public float JumpHeight = 5.0f;

        public Transform _groundChecker;
        public float GroundDistance = 1.0f;

        public Transform ObjectHolder;
        public float HoldableSpeed;
        public float HoldableShootForce = 10.0f;


        public int MaxHp = 100;

        private Rigidbody _rigidbody;

        private Holdable _holdable;
        private Camera _camera;

        public int Hp { get; private set; }

        // Use this for initialization
        void Start() {          
            _rigidbody = GetComponent<Rigidbody>();
            _camera = Camera.main;

            Hp = MaxHp;
        }

        private void Update() {
            if (Input.GetMouseButtonDown(0)) {
                TryHold();
            }
        }

        void FixedUpdate() {
            var move = (_camera.transform.right * Input.GetAxis("Horizontal") +
                        _camera.transform.forward * Input.GetAxis("Vertical"));
            move.y = 0;
            var speed = _holdable ? HoldSpeed : Speed;
            _rigidbody.velocity = move.normalized * Time.deltaTime * speed + _rigidbody.velocity.y * Vector3.up;

            if (Input.GetButtonDown("Jump")) {
                Jump();
            }

            Rotate();
        }

        private void Rotate() {
            transform.LookAt(_camera.transform.position + (transform.position - _camera.transform.position) * 2);
            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);
        }


        private float _lastJumpTime;

        private void Jump() {
            var mask = LayerMask.GetMask("Ground");
            var isGrounded = Physics.CheckSphere(_groundChecker.position, GroundDistance, mask,
                QueryTriggerInteraction.Ignore);
            if (isGrounded && Time.time - _lastJumpTime > 0.3f) {
                _lastJumpTime = Time.time;
                _rigidbody.AddForce(Vector3.up * JumpHeight);
            }
        }

        private void TryHold() {
            if (_holdable != null) {
                var force = (_holdable.transform.position - _camera.transform.position).normalized * HoldableShootForce;
                force.y = 0;
                _holdable.Shoot(force);
                Drop();
                return;
            }

            RaycastHit hit;
            var ray = _camera.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f));
            if (Physics.Raycast(ray, out hit)) {
                Transform objectHit = hit.transform;
                var holdable = objectHit.GetComponent<Holdable>();
                if (holdable) {
                    holdable.OnHold(ObjectHolder, HoldableSpeed);
                    _holdable = holdable;
                    MusicManager.GetInstance().PlayHold();
                    return;
                }

                var enemy = objectHit.GetComponent<Navigator>();
                if (enemy) {
                    enemy.Stun();
                }
            }
        }

        public void Drop() {
            MusicManager.GetInstance().Drop();
            _holdable = null;
        }

        public bool IsHolding(Holdable holdable) {
            return holdable.Equals(_holdable);
        }

        public bool IsHoldingAnything() {
            return _holdable != null;
        }

        public void TakeDamage(Projectile projectile) {
            Hp -= projectile.Damage;
            if (Hp <= 0) {
                UiManager.GetInstance().OnDeath();
            }
        }
    }
}