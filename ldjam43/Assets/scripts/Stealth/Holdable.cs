using System;
using UnityEngine;

namespace Stealth {
    public class Holdable : MonoBehaviour {

        public GameObject[] DeactivateOnPickup;
        
        private float _speed;
        private Transform _target;
        
        public void OnHold(Transform target, float speed) {
            _target = target;
            _speed = speed;
            GetComponent<Rigidbody>().isKinematic = true;
            
            if (DeactivateOnPickup == null) return;
            foreach (var obj in DeactivateOnPickup) {
                obj.SetActive(false);
            }
        }

        private void FixedUpdate() {
            if (_target == null) return;
            
            transform.position = Vector3.Lerp(transform.position, _target.position, Time.deltaTime * _speed);
        }

        public void Shoot(Vector3 force) {
            _target = null;

            var body = GetComponent<Rigidbody>();
            body.isKinematic = false;
            body.AddForce(force);  
        }
    }
}