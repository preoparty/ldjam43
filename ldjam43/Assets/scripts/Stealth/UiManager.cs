using Mechanics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Stealth {
    public class UiManager : AbstarctEnablingSigleton<UiManager> {
        public Text HpText; 
        public Text KilledText;

        public Transform DeathPanel;
        public Transform VictoryPanel;

        private void Start() {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1.0f;
        }

        private void Update() {
            HpText.text = Character.GetInstance().Hp.ToString();
            KilledText.text = EnemyDeathTrigger.KilledCount + "/" + EnemyDeathTrigger.MaxCount;
        }

        public void OnDeath() {
            ShowCursorAndPause();
            DeathPanel.gameObject.SetActive(true);
        }

        private static void ShowCursorAndPause() {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0.0f;
        }

        public void OnVictory() {
            ShowCursorAndPause();
            VictoryPanel.gameObject.SetActive(true);
        }

        public void Reload() {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}