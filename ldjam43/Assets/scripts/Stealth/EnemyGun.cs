﻿using UnityEngine;

namespace Stealth {
	public class EnemyGun : MonoBehaviour {

		public Transform Spawn;
		public Rigidbody Projectile;
		public float FireRate = 1.0f;
		public float ShootForce = 100.0f;
		
		private bool _shouldShoot;

		private float _lastShootTime;

		private Character _player;

		// Use this for initialization
		void Start () {
			_player = Character.GetInstance();
		}
	
		// Update is called once per frame
		void FixedUpdate () {
			if (_shouldShoot && Time.time - _lastShootTime >= FireRate) {
				var newProjectile = Instantiate(Projectile);
				newProjectile.transform.position = Spawn.position;
				newProjectile.transform.LookAt(_player.transform);
				_lastShootTime = Time.time;
				newProjectile.AddForce((_player.transform.position - newProjectile.transform.position).normalized * ShootForce);
			}
		}

	
		public void StartShooting() {
			_shouldShoot = true;
		}
	
		public void StopShooting() {
			_shouldShoot = false;
		}
	}
}
