using UnityEngine;

namespace Stealth {
    public class StunEnablingCollider : MonoBehaviour {
        private Navigator parent;
        private void Start() {
            parent = transform.parent.GetComponent<Navigator>();
        }

        private void OnEnable() {
            SetColor(Color.white);
        }

        private void OnTriggerEnter(Collider other) {
			if (other.CompareTag ("Player")) {
				parent.Stunnable = true;
				SetColor (Color.green);
			}
        }

        private void OnTriggerExit(Collider other) {
			if (other.CompareTag("Player")) {
                parent.Stunnable = false;
                SetColor(Color.white);
			} else if (other.CompareTag ("Throwable")) {
				parent.Stunnable = true;
				parent.Stun ();
			}
        }
        
        private void SetColor(Color color) {
            var meshRenderer = GetComponent<MeshRenderer>();
            meshRenderer.materials[0].SetColor("_Color", color);
        }
    }
}