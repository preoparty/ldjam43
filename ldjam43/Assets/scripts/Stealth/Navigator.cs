﻿using System.Collections;
using System.Collections.Generic;
using Stealth;
using UnityEngine;
using UnityEngine.AI;

public class Navigator : MonoBehaviour {
    public Transform[] points;

    public float minDistance;
    public float chaseDistance = 3f;
    
    public float StunTime = 10.0f;

    public float StunPushForce = 100f;
    public float CharPushForce = 1000f;

    private int _currentPoint;
    private NavMeshAgent _agent;
    private Transform _followTarget;
	private Voice _voice;

    private bool _stunned;
    public bool Stunnable { get; set; }
    private float _stunStartTime;



    void Start() {
        _agent = GetComponent<NavMeshAgent>();
        _agent.SetDestination(points[_currentPoint].position);
		_voice = GetComponent<Voice> ();
    }

    // Update is called once per frame
    void Update() {
        if (_stunned) TryWakeUp();
        if (_followTarget != null) {
            ChaseMovement();
        }
        else {
            RouteMovement();
        }
    }

    void RouteMovement() {
        if (Vector3.Distance(transform.position, points[_currentPoint].position) < minDistance) {
            _currentPoint++;
            if (_currentPoint >= points.Length)
                _currentPoint = 0;
            _agent.SetDestination(points[_currentPoint].position);
        }
    }

    public void Stun() {
        if (!Stunnable) return;
        _agent.enabled = false;
        _stunned = true;
		_voice.OnStun();
        var vision = GetComponentInChildren<EnemyVision>();
        vision.Reset();
        vision.gameObject.SetActive(false);
        GetComponentInChildren<StunEnablingCollider>().gameObject.SetActive(false);
        var body = GetComponent<Rigidbody>();
        body.isKinematic = false;
        body.AddForce((transform.position - Character.GetInstance().transform.position).normalized * StunPushForce);
        gameObject.AddComponent<Holdable>();
        StopChase();
        _stunStartTime = Time.time;
    }

    public void TryWakeUp() {
        if (!_stunned) return;

        if (Time.time - _stunStartTime > StunTime) {

            
            var body = GetComponent<Rigidbody>();
            body.isKinematic = true;
            transform.eulerAngles = Vector3.zero;
            _agent.enabled = true;
            _agent.SetDestination(points[_currentPoint].position);

            var vision = GetComponentInChildren<EnemyVision>(true);
            vision.gameObject.SetActive(true);
            GetComponentInChildren<StunEnablingCollider>(true).gameObject.SetActive(true);

            vision.Reset();
            Stunnable = false;

            var character = Character.GetInstance();
            if (character.IsHolding(GetComponent<Holdable>())) {
                var forceVector = (character.transform.position - transform.position).normalized * CharPushForce;
                character.GetComponent<Rigidbody>()
                    .AddForce(new Vector3(forceVector.x, 0.0f, forceVector.z));
                character.Drop();
                vision.OnPlayerDetected(character.transform, true);
                _voice.OnChase();
            } else {
               _voice.OnLost(); 
            }

            Destroy(gameObject.GetComponent<Holdable>());
            _stunned = false;
        }
    }

    void OnCollisionEnter(Collision collision) {
        print(collision.collider.ToString() + collision.relativeVelocity.magnitude);
    }

    void ChaseMovement() {
        if (_followTarget != null) {
            if (Vector3.Distance(transform.position, _followTarget.position) < chaseDistance) {
                _agent.SetDestination(_followTarget.position);
            }
            else {
                StopChase();
            }
        }
    }

    public bool IsChasing() {
        return _followTarget != null;
    }

    public void StopChase() {
        if (_followTarget == null) return;
        GetComponentInChildren<EnemyVision>().Reset();
        GetComponent<EnemyGun>().StopShooting();
        _followTarget = null;
        _agent.SetDestination(points[_currentPoint].position);
        _voice.OnLost();
        MusicManager.GetInstance().StopChase();
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, chaseDistance);
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position, points[0].position);
        for (var i = 0; i < points.Length; i++) {
            Gizmos.DrawLine(points[i].position, points[(i + 1) % points.Length].position);
        }
    }

    private void OnDestroy() {
        if (IsChasing()) {
            MusicManager.GetInstance().StopChase();
            return;
        }

        var holdable = GetComponent<Holdable>();
        if (holdable) {
            MusicManager.GetInstance().Drop();
        }
    }

    public void SetTarget(Transform target) {
        MusicManager.GetInstance().PlayChase();
        _followTarget = target;
        GetComponent<EnemyGun>().StartShooting();
        _voice.OnChase();
    }
}