using UnityEngine;

namespace Stealth {
    public class CoverTrigger : MonoBehaviour {
        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag("Player")) {
                var enemies = FindObjectsOfType<Navigator>();
                foreach (var navigator in enemies) {
                    navigator.StopChase();
                }
            }
        }
    }
}