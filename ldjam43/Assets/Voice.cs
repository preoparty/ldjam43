﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Voice : MonoBehaviour {
	public StateClips[] StateAudio;
	public AudioClip[] FoundClips;
	public AudioClip[] LostClips;

	public float Frequency = 5.0f;
	public float MaxStartDelay = 5.0f;
	
	private AudioSource _audio;
	private float _lastAudioTime;

	private EnemyState _state;
	// Use this for initialization
	void Start () {
		_lastAudioTime = Random.Range(0.0f, MaxStartDelay);
		_audio = GetComponent<AudioSource>();
		_state = EnemyState.Idle;
		_audio.pitch = Random.Range (0.75f, 1.25f);
	}
	
	// Update is called once per frame
	private void Update() {
		if (Time.time - _lastAudioTime  > Frequency) {
			PlaySound(StateAudio.First(a => a.State == _state).Clips);
		}
	}

	public void OnChase() {
		_state = EnemyState.Chase;
	}

	public void OnFound() {
		PlaySound(FoundClips);
	}
	
	public void OnLost() {
		PlaySound(LostClips);
		_state = EnemyState.Idle;
	}

	public void OnStun() {
		_state = EnemyState.Stun;
	}

	public void PlaySound(AudioClip[] clips) {
		_audio.clip = clips[Random.Range(0, clips.Length)];
		_audio.Play ();
		if (_lastAudioTime <= Time.time) {
			_lastAudioTime = Time.time;			
		}
	}
	
	[Serializable]
	public enum EnemyState {
		Idle, Stun, Chase
	}
	
	[Serializable]
	public class StateClips {
		public EnemyState State;
		public AudioClip[] Clips;
	}
}
