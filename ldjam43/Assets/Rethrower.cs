﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rethrower : MonoBehaviour {
	public Transform player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other) {
		other.gameObject.transform.position = player.position + (Vector3.up * 10);

		if (other.GetComponent<Navigator>()) {
			other.GetComponent<Navigator> ().Stun ();
		}
	}
}
